/*---- turns out i am allergic to calamari ----*/
let tabBody = "//glen-themes.gitlab.io/abt-pages/15/status-tab-get.html";

fetch(tabBody)
.then(response => response.text())
.then(bv => {
  let ttab = document.querySelector("#status-tab");
  
  if(ttab){
    ttab.innerHTML = bv;
    ttab.style.visibility = "visible";
  }
  
  waitForElement(".sun-s", { end: 1500 }).then(() => {
    let suns = document.querySelector(".sun-s");
    if(suns){
      let icon_url = getComputedStyle(document.documentElement).getPropertyValue("--StatusTab-Image-URL");
      let img = document.createElement("img");
      img.classList.add("icon-image");
      img.src = icon_url;
      suns.append(img)
    }
    
    let tt = document.querySelector(".the-text");
    if(tt){
      let the_text = getComputedStyle(document.documentElement).getPropertyValue("--StatusTab-Text");
      tt.textContent = the_text
    }
  }).catch(err => console.error(err));

  
})
.catch(err => console.error(err));
